
from __future__ import print_function

import six
import time
import json
import os

from qtpy import QtWidgets, QtCore

try:
    from kabaret.app.ui.gui.widgets.widget_view import DockedView
except ImportError:
    raise RuntimeError('Your kabaret version is too old, for the JobView. Please update !')

from kabaret.app import resources

class JobListParamItem(QtWidgets.QTreeWidgetItem):

    def __init__(self, job_item, v, k=None):
        super(JobListParamItem, self).__init__(job_item)
        self._job_item = job_item
        self.k = k
        self.v = v 

        if k is not None:
            self.setText(0, '{}={}'.format(k, v))
        else:
            self.setText(0, v)

        self.setIcon(0, resources.get_icon(('icons.gui', 'tag-black-shape')))

    def job_item(self):
        return self._job_item

class JobListItem(QtWidgets.QTreeWidgetItem):

    def __init__(self, tree, job):
        super(JobListItem, self).__init__(tree)
        self.job = None
        self.args = ()
        self.kwargs = {}
        self._match_str = ''
        self.set_job(job)

    def job_item(self):
        return self

    def set_job(self, job):
        nb = self.childCount()
        for i in range(nb):
            child = self.takeChild(i)
            # Destroy it ?

        self.job = job
        self.args = ()
        self.kwargs = {}
        self._match_str = ''
        self._update()

    def _update(self):
        j = self.job
        self.setText(0, j['job_type'])
        self.setText(1, str(j['job_params']))
        self.setText(2, j['owner'])
        self.setText(3, j['pool'])
        self.setText(4, str(j['priority']))
        self.setText(5, j.get('node', ''))

        self.setText(6, str(j['paused']))
        self.setText(7, str(j['in_progress']))
        self.setText(8, str(j['done']))

        self.setText(9, j['creator'])
        
        ts = j['created_on']
        date_str = time.ctime(ts)

        self.setText(10, date_str)
        self.setText(11, str(ts))
        self.setText(12, j['jid'])

        status = j['status']
        self.setIcon(0, resources.get_icon(j['icon_ref']))
        
        matchers = [
            j['jid'],
            j['owner'], status, j['job_type'],
            j['creator'], date_str
        ]

        self.args, self.kwargs = json.loads(j['job_params'])
        for v in self.args:
            child = JobListParamItem(self, v)
            matchers.append(v)

        for k, v in self.kwargs.items():
            child = JobListParamItem(self, v, k)
            matchers.append('{}={}'.format(k,v))

        self._match_str = '^'.join(matchers).lower()

    def jid(self):
        return self.job['jid']

    def paused(self):
        return self.job['paused']

    def in_progress(self):
        return self.job['in_progress']

    def done(self):
        return self.job['done']

    def matches(self, filter):
        return filter in self._match_str

class JobList(QtWidgets.QTreeWidget):

    def __init__(self, parent, session):
        super(JobList, self).__init__(parent)
        self.session = session

        columns = (
            'Type', 'Params', 
            'Owner', 
            'Pool', 'Priority', 
            'Node', 

            'Paused', 'In Progress', 'Done', 
            
            'Creator',
            'Created On', 

            'TIMESTAMP', 'ID'
        )
        self.setHeaderLabels(columns)
        
        self.setColumnHidden(1, True) # Params

        self.setColumnHidden(6, True) # Done
        self.setColumnHidden(7, True) # In Progress
        self.setColumnHidden(8, True) # Paused

        self.setColumnHidden(len(columns)-1, True) # ID
        self.setColumnHidden(len(columns)-2, True) # TIMESTAMP


        self.setSortingEnabled(True)
        self.sortByColumn(len(columns)-2, QtCore.Qt.DescendingOrder)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._on_popup_menu_request)

        self._popup_menu = QtWidgets.QMenu(self)

        self._filter = None
        self._jid_to_item = {}

    def set_item_filter(self, filter):
        self._filter = filter
        self.apply_filter()

    def refresh(self):
        #TODO: intelligent refresh: remove deleted jids, add created jids
        self._jid_to_item.clear()
        self.clear()
        for job in self.session.cmds.Jobs.list_jobs():
            item = JobListItem(self, job)
            self._jid_to_item[item.jid()] = item
            if self._filter and not item.matches(self._filter):
                item.setHidden(True)

    def apply_filter(self):
        root = self.invisibleRootItem()
        nb = root.childCount()
        for i in range(nb):
            item = root.child(i)
            if not self._filter:
                item.setHidden(False)
            else:
                item.setHidden(
                    not item.matches(self._filter)
                )

    def refresh_job(self, jid):
        item = self._jid_to_item.get(jid)
        if item is not None:
            job = self.session.cmds.Jobs.get_job_info(jid)
            item.set_job(job)

    def _on_popup_menu_request(self, pos):
        item = self.itemAt(pos)
        if item is None:
            m = self._popup_menu
            m.clear()
            m.addAction('Refresh', self.refresh)

        else:
            item = item.job_item()
            m = self._popup_menu
            m.clear()
            if item.paused():
                m.addAction('Un-Pause', lambda item=item: self._unpause(item))
            if 'oid' in item.kwargs:
                oid = item.kwargs['oid']
                name = os.path.basename(oid)
                m.addAction('Goto "{}"'.format(name), lambda item=item: self._goto(item))
            m.addAction('Delete', lambda item=item: self._delete(item))

        self._popup_menu.popup(self.viewport().mapToGlobal(pos))

    def _unpause(self, item):
        self.session.cmds.Jobs.set_job_paused(item.jid(), False)

    def _delete(self, item):
        self.session.cmds.Jobs.delete_job(item.jid())

    def _goto(self, item):
        if not self.session.is_gui():
            return

        oid = str(item.kwargs['oid'])
        wm = self.session.main_window_manager
        view = wm.add_view('Flow', area=None, oid=oid)
        # ensure visible if tabbed:
        view.show()

class JobsView(DockedView):

    @classmethod
    def view_type_name(cls):
        return 'Jobs'

    def __init__(self, *args, **kwargs):
        super(JobsView, self).__init__(*args, **kwargs)

    def _build(self, top_parent, top_layout, main_parent, header_parent, header_layout):
        self.add_header_tool('*', '*', 'Duplicate View', self.duplicate_view)

        self._filter_le = QtWidgets.QLineEdit(main_parent)
        self._filter_le.setPlaceholderText('Filter Jobs...')
        self._filter_le.textChanged.connect(self._on_filter_change)

        self._job_list = JobList(main_parent, self.session)

        lo = QtWidgets.QVBoxLayout()
        lo.setContentsMargins(0, 0, 0, 0)
        lo.setSpacing(0)
        lo.addWidget(self._filter_le)
        lo.addWidget(self._job_list)

        main_parent.setLayout(lo)


        self.view_menu.setTitle('Jobs')

        a = self.view_menu.addAction('Refresh', self.refresh)
        self.view_menu.addSeparator()
        a = self.view_menu.addAction('Create Test Job', self.create_test_job)

    def _on_filter_change(self):
        self._job_list.set_item_filter(self._filter_le.text().lower())

    def refresh(self):
        self._job_list.refresh()

    def create_test_job(self):
        jid = self.session.cmds.Jobs.create_flow_job(
            oid='/project/path/to/the/job_object',
            pool='test', 
            priority=50,
            paused=True,
        )
        print('NEW JOB ID:', jid)

    def on_show(self):
        self.refresh()

    def receive_event(self, event, data):
        if not self.isVisible():
            return

        if event == 'joblist_touched':
            self._job_list.refresh() 

        elif event == 'job_touched':
            jid = data['jid']
            self._job_list.refresh_job(jid)